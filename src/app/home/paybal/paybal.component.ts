import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ICreateOrderRequest, IPayPalConfig} from 'ngx-paypal';
import {Fashionboard} from '../../models/fashionboard.model';
import {DataService} from '../../services/data.service';

@Component({
    selector: 'app-paybal',
    templateUrl: './paybal.component.html',
    styleUrls: ['./paybal.component.scss']
})
export class PaybalComponent implements OnInit {

    showSuccess: boolean;
    public payPalConfig?: IPayPalConfig;
    @Input() fashionBoard: Fashionboard;
    @Output() purchaseActionPaybal = new EventEmitter<any>();
    paymentId: string;

    constructor(public dataService: DataService) {
    }

    ngOnInit(): void {
        this.initConfig();
    }

    private initConfig(): void {
        this.payPalConfig = {
            currency: 'EUR',
            clientId: 'sb',
            createOrderOnClient: (data) => <ICreateOrderRequest> {
                intent: 'CAPTURE',
                purchase_units: [
                    {
                        amount: {
                            currency_code: 'EUR',
                            value: this.fashionBoard.price.toString(),
                            breakdown: {
                                item_total: {
                                    currency_code: 'EUR',
                                    value: this.fashionBoard.price.toString()
                                }
                            }
                        },
                        items: [
                            {
                                name: 'Enterprise Subscription',
                                quantity: '1',
                                category: 'DIGITAL_GOODS',
                                unit_amount: {
                                    currency_code: 'EUR',
                                    value: this.fashionBoard.price.toString()
                                },
                            }
                        ]
                    }
                ]
            },
            advanced: {
                commit: 'true'
            },
            style: {
                label: 'paypal',
                layout: 'vertical'
            },
            onApprove: (data, actions) => {
                console.log('onApprove - transaction was approved, but not authorized', data, actions);
                actions.order.get().then(details => {
                    console.log('onApprove - you can get full order details inside onApprove: ', details);
                });
            },
            onClientAuthorization: (data) => {
                console.log('onClientAuthorization - you should probably inform your server about completed transaction at this point', data);
                this.showSuccess = true;
                this.dataService.paypalPayment({'fashionBundle': this.fashionBoard, 'paybal': data}).subscribe(res => {
                    this.purchaseActionPaybal.emit(res);
                });
            },
            onCancel: (data, actions) => {
                console.log('OnCancel', data, actions);
            },
            onError: err => {
                console.log('OnError', err);
            },
            onClick: (data, actions) => {
                console.log('onClick', data, actions);
            },
        };
    }

}
