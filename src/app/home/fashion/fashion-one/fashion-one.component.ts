import {Component, OnInit} from '@angular/core';
import {ProductSlider, TestimonialSlider} from '../../../shared/data/slider';
import {Product} from '../../../shared/classes/product';
import {ProductService} from '../../../shared/services/product.service';
import {ActivatedRoute, Router} from '@angular/router';
import {TokenStorageService} from '../../../services/token-storage.service';
import {AuthService} from '../../../services/auth.service';

@Component({
    selector: 'app-fashion-one',
    templateUrl: './fashion-one.component.html',
    styleUrls: ['./fashion-one.component.scss']
})
export class FashionOneComponent implements OnInit {
    public sessionStorage = sessionStorage;

    public products: Product[] = [];
    public productCollections: any[] = [];
    public TestimonialSliderConfig: any = TestimonialSlider;
    public testimonial = [{
        image: 'assets/images/testimonial/1.jpg',
        name: 'Mark jkcno',
        designation: 'Designer',
        description: 'you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings.',
    }, {
        image: 'assets/images/testimonial/2.jpg',
        name: 'Adegoke Yusuff',
        designation: 'Content Writer',
        description: 'you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings.',
    }, {
        image: 'assets/images/testimonial/1.jpg',
        name: 'John Shipmen',
        designation: 'Lead Developer',
        description: 'you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings.',
    }];
    public blogs = [{
        image: 'assets/images/blog/comment1.jpg',
        date: 'Passer par un Quiz',
        title: 'pour mieux connaitre votre style et le type de tenu que vous cherchiez',
        by: ''
    }, {
        image: 'assets/images/blog/comment2.jpg',
        date: 'Recevez votre Style Board',
        title: 'pour mieux connaitre votre style et le type de tenu que vous cherchiez',
        by: ''
    }, {
        image: 'assets/images/blog/comment3.jpg',
        date: 'Faites votre shopping',
        title: 'Vous pouvez achetez directement les articles des sites Webs des marques.\n' +
            'Vous achetez ce que vous aimez',
        by: ''
    }, {
        image: 'assets/images/blog/37.jpg',
        date: '28 January 2018',
        title: 'Lorem ipsum dolor sit consectetur adipiscing elit,',
        by: 'John Dio'
    }];
    public blogsFashionBoard = [{
        image: 'assets/images/blog/fashion1.png',
        date: '',
        title: '',
        by: ''
    }, {
        image: 'assets/images/blog/fashion2.png',
        date: '',
        title: '',
        by: ''
    }, {
        image: 'assets/images/blog/fashion3.png',
        date: '',
        title: '',
        by: ''
    }, {
        image: 'assets/images/blog/37.jpg',
        date: '28 January 2018',
        title: 'Lorem ipsum dolor sit consectetur adipiscing elit,',
        by: 'John Dio'
    }];

    constructor(public productService: ProductService, private route: ActivatedRoute, private router: Router, public tokenStorage: TokenStorageService, public authService: AuthService) {
        this.route.queryParams
            .subscribe(params => {
                    if (params.token) {
                        this.tokenStorage.saveToken(params.token);
                    }
                }
            );
        this.router.navigate([], {
            queryParams: {
                token: null,
            },
            queryParamsHandling: 'merge'
        });
        if (this.tokenStorage.getToken()){
            this.authService.me().subscribe(res => {
                this.tokenStorage.saveUser(res);
            });
        }
        this.productService.getProducts.subscribe(response => {
            this.products = response.filter(item => item.type == 'fashion');
            // Get Product Collection
            this.products.filter((item) => {
                item.collection.filter((collection) => {
                    const index = this.productCollections.indexOf(collection);
                    if (index === -1) {
                        this.productCollections.push(collection);
                    }
                });
            });
        });
    }

    public ProductSliderConfig: any = ProductSlider;

    public sliders = [{
        title: ' vous permet de dénicher les pièces dont vous avez besoin, plus besoin de faire le\n' +
            '                                    tour des magasins et des sites web pour trouver la petite robe noire ou la tenue du\n' +
            '                                    samedi soir',
        subTitle: 'Rise With Style',
        image: 'assets/images/slider/slider1.jpg'
    }, {
        title: 'Un styliste expérimenté vous sélectionnera des pièces adéquates à votre morphologie\n' +
            '                                    et style et en un click vous pourriez les acheter directement du site de la marque.',
        subTitle: 'Rise With Style',
        image: 'assets/images/slider/slider2.jpg'
    }];

    // Collection banner
    public collections = [{
        image: 'assets/images/collection/fashion/1.jpg',
        save: 'save 50%',
        title: 'men'
    }, {
        image: 'assets/images/collection/fashion/2.jpg',
        save: 'save 50%',
        title: 'women'
    }];

    // Blog
    public blog = [{
        image: 'assets/images/blog/1.jpg',
        date: '25 January 2018',
        title: 'Lorem ipsum dolor sit consectetur adipiscing elit,',
        by: 'John Dio'
    }, {
        image: 'assets/images/blog/2.jpg',
        date: '26 January 2018',
        title: 'Lorem ipsum dolor sit consectetur adipiscing elit,',
        by: 'John Dio'
    }, {
        image: 'assets/images/blog/3.jpg',
        date: '27 January 2018',
        title: 'Lorem ipsum dolor sit consectetur adipiscing elit,',
        by: 'John Dio'
    }, {
        image: 'assets/images/blog/4.jpg',
        date: '28 January 2018',
        title: 'Lorem ipsum dolor sit consectetur adipiscing elit,',
        by: 'John Dio'
    }];

    // Logo
    public logo = [{
        image: 'assets/images/logos/1.png',
    }, {
        image: 'assets/images/logos/2.png',
    }, {
        image: 'assets/images/logos/3.png',
    }, {
        image: 'assets/images/logos/4.png',
    }, {
        image: 'assets/images/logos/5.png',
    }, {
        image: 'assets/images/logos/6.png',
    }, {
        image: 'assets/images/logos/7.png',
    }, {
        image: 'assets/images/logos/8.png',
    }];

    ngOnInit(): void {

    }

    // Product Tab collection
    getCollectionProducts(collection) {
        return this.products.filter((item) => {
            if (item.collection.find(i => i === collection)) {
                return item;
            }
        });
    }

}
