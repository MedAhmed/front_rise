import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {HomeRoutingModule} from './home-routing.module';

import {FashionOneComponent} from './fashion/fashion-one/fashion-one.component';
import {FashionTwoComponent} from './fashion/fashion-two/fashion-two.component';
import {FashionThreeComponent} from './fashion/fashion-three/fashion-three.component';
import {VegetableComponent} from './vegetable/vegetable.component';
import {WatchComponent} from './watch/watch.component';
import {FurnitureComponent} from './furniture/furniture.component';
import {FlowerComponent} from './flower/flower.component';
import {BeautyComponent} from './beauty/beauty.component';
import {ElectronicsComponent} from './electronics/electronics.component';
import {PetsComponent} from './pets/pets.component';
import {GymComponent} from './gym/gym.component';
import {ToolsComponent} from './tools/tools.component';
import {ShoesComponent} from './shoes/shoes.component';
import {BagsComponent} from './bags/bags.component';
import {MarijuanaComponent} from './marijuana/marijuana.component';

// Widgest Components
import {SliderComponent} from './widgets/slider/slider.component';
import {BlogComponent} from './widgets/blog/blog.component';
import {LogoComponent} from './widgets/logo/logo.component';
import {ServicesComponent} from './widgets/services/services.component';
import {CollectionComponent} from './widgets/collection/collection.component';
import {FashionBundleComponent} from './fashion-bundle/fashion-bundle.component';
import {FashionBoardClearComponent} from './fashion-board-clear/fashion-board-clear.component';
import {FashionBoardPurchaseComponent} from './fashion-board-purchase/fashion-board-purchase.component';
import {MatStepperModule} from '@angular/material/stepper';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {NgxIntlTelInputModule} from 'ngx-intl-tel-input';
import {ApiService} from '../services/api.service';
import { PaybalComponent } from './paybal/paybal.component';
import {NgxPayPalModule} from 'ngx-paypal';
import { UserProfileComponent } from './user-profile/user-profile.component';
import {UserProfileModule} from './user-profile/user-profile.module';

@NgModule({
        declarations: [
            FashionOneComponent,
            FashionTwoComponent,
            FashionThreeComponent,
            VegetableComponent,
            WatchComponent,
            FurnitureComponent,
            FlowerComponent,
            BeautyComponent,
            ElectronicsComponent,
            PetsComponent,
            GymComponent,
            ToolsComponent,
            ShoesComponent,
            BagsComponent,
            MarijuanaComponent,
            // Widgest Components
            SliderComponent,
            BlogComponent,
            LogoComponent,
            ServicesComponent,
            CollectionComponent,
            FashionBundleComponent,
            FashionBoardClearComponent,
            FashionBoardPurchaseComponent,
            PaybalComponent,
            UserProfileComponent,
        ],
    imports: [
        CommonModule,
        HomeRoutingModule,
        SharedModule,
        MatStepperModule,
        MatFormFieldModule,
        MatButtonModule,
        MatInputModule,
        NgxPayPalModule,
        NgxIntlTelInputModule,
        UserProfileModule
    ],
        providers: [ApiService]
    , entryComponents:
[FashionBundleComponent]
})

export class HomeModule {
}
