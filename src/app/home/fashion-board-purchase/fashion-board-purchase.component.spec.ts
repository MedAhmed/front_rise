import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FashionBoardPurchaseComponent } from './fashion-board-purchase.component';

describe('FashionBoardPurchaseComponent', () => {
  let component: FashionBoardPurchaseComponent;
  let fixture: ComponentFixture<FashionBoardPurchaseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FashionBoardPurchaseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FashionBoardPurchaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
