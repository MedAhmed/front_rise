import {Component, ElementRef, Input, NgZone, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CountryISO, PhoneNumberFormat, SearchCountryField} from 'ngx-intl-tel-input';
import {ApiService, Maps} from '../../services/api.service';
import {MatStepper} from '@angular/material/stepper';
import {AuthService} from '../../services/auth.service';
import {Elements, StripeService, Element as StripeElement, ElementsOptions} from 'ngx-stripe';
import {DataService} from '../../services/data.service';
import {Fashionboard} from '../../models/fashionboard.model';
import {ToastrService} from 'ngx-toastr';

@Component({
    selector: 'app-fashion-board-purchase',
    templateUrl: './fashion-board-purchase.component.html',
    styleUrls: ['./fashion-board-purchase.component.scss']
})
export class FashionBoardPurchaseComponent implements OnInit {
    @Input() user;
    @ViewChild('stepper', {static: false}) stepper: MatStepper;
    isLinear = false;
    paymentId:string;
    submitted: boolean = false;
    firstFormGroup: FormGroup;
    secondFormGroup: FormGroup;
    separateDialCode = false;
    SearchCountryField = SearchCountryField;
    CountryISO = CountryISO;
    PhoneNumberFormat = PhoneNumberFormat;
    preferredCountries: CountryISO[] = [CountryISO.UnitedStates, CountryISO.UnitedKingdom];
    @Input() fashionBoard: Fashionboard;
    elements: Elements;
    card: StripeElement;
    paymentStatus: any;
    stripeData: any;
    submittedFormTwo: any;
    loading: any;
    elementsOptions: ElementsOptions = {
        locale: 'fr'
    };
    galleryFilter: string = 'Card';

    constructor(apiService: ApiService, private formBuilder: FormBuilder, private ngZone: NgZone,
                public authService: AuthService, private stripeService: StripeService, public dataService: DataService, public toastr: ToastrService) {
        apiService.api.then(maps => {
            this.initAutocomplete(maps);
        });
    }

    @ViewChild('search')
    public searchElementRef: ElementRef;

    ngOnInit() {
        const firstName = this.user.firstName;
        console.log(firstName);

        const lastName = this.user.lastName;
        this.firstFormGroup = this.formBuilder.group({
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            phone: ['', Validators.required],
            place: ['', Validators.required]

        });
        this.firstFormGroup.get('firstName').setValue(firstName);
        this.firstFormGroup.get('lastName').setValue(lastName);
        this.firstFormGroup.get('place').setValue(this.user.place);
        this.firstFormGroup.get('phone').setValue(this.user.phoneNumber);

        this.secondFormGroup = this.formBuilder.group({
            name: ['', Validators.required],
        });
        this.generateCard();
    }

    initAutocomplete(maps: Maps) {
        let autocomplete = new maps.places.Autocomplete(this.searchElementRef.nativeElement);
        autocomplete.addListener('place_changed', () => {
            this.ngZone.run(() => {
            });
        });
    }

    get f() {
        return this.firstFormGroup.controls;

    }

    get f2() {
        return this.firstFormGroup.controls;

    }

    nextForm() {
        this.submitted = true;
        if (this.firstFormGroup.invalid) {
            return;
        } else {
            console.log(this.firstFormGroup.get('phone').value);
            const v = {
                ...this.firstFormGroup.getRawValue(),
                phone: this.firstFormGroup.get('phone').value.internationalNumber
            };
            console.log(v);
            this.authService.changeMe(v).subscribe(res => {
                console.log(res);
            });
            this.stepper.next();
        }
    }

    generateCard() {
        this.loading = false;
        this.stripeService.elements(this.elementsOptions).subscribe(elements => {
            this.elements = elements;
            if (!this.card) {
                this.card = this.elements.create('card', {
                    iconStyle: 'solid',
                    style: {
                        base: {
                            iconColor: '#666EE8',
                            color: '#31325F',
                            lineHeight: '40px',
                            fontWeight: 300,
                            fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                            fontSize: '18px',
                            '::placeholder': {
                                color: '#CFD7E0'
                            }
                        }
                    }
                });
                this.card.mount('#card-element');
            }
        });
    }

    buy() {
        if (this.secondFormGroup.invalid) {
            return;
        } else {
            this.submittedFormTwo = true;
            this.loading = true;
            this.stripeData = this.secondFormGroup.value;
            this.stripeData = {...this.stripeData, fashionBundle: this.fashionBoard};
            this.stripeService.createToken(this.card, {name: this.secondFormGroup.get('name').value}).subscribe(result => {
                if (result.token) {
                    this.stripeData['token'] = result.token;
                    this.stripeData['amount'] = this.fashionBoard.price;

                    this.dataService.stripePayment(this.stripeData).subscribe((res) => {
                        if (res['success']) {
                            this.loading = false;
                            this.submittedFormTwo = false;
                            this.paymentStatus = res['status'];
                            this.toastr.success(this.paymentStatus, 'success');
                        } else {
                            this.loading = false;
                            this.submittedFormTwo = false;
                            this.paymentStatus = res['status'];
                            this.toastr.success(this.paymentStatus, 'success');

                        }
                        this.paymentId = res['id'];
                        this.stepper.next();
                    });
                } else if (result.error) {
                    this.toastr.error(result.error.message, 'alert');
                }
            });
        }
    }

    filterMenu(all: string) {
        this.galleryFilter = all;
    }

    purchaseActionPaybal($event: any) {
        this.paymentId = $event['id'];
        this.toastr.success($event.paymentStatus, 'success');
        this.stepper.next();
    }

    nothing() {

    }
}
