import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FashionboardService} from '../../services/fashionboard.service';
import {Fashionboard} from '../../models/fashionboard.model';

@Component({
    selector: 'app-fashion-bundle',
    templateUrl: './fashion-bundle.component.html',
    styleUrls: ['./fashion-bundle.component.scss']
})
export class FashionBundleComponent implements OnInit {
    public fashionBoards: Fashionboard[] = [];
    @Output() purchaseAction = new EventEmitter<any>();

    constructor(public fashionBoardService: FashionboardService) {
    }

    ngOnInit(): void {
        this.fashionBoardService.getAllFashionBoard().subscribe(res => {
            this.fashionBoards = res;
        });
    }

    purchaseFashionBundle(fashionboard: Fashionboard) {
        this.purchaseAction.emit(fashionboard);
    }
}
