import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FashionBundleComponent } from './fashion-bundle.component';

describe('FashionBundleComponent', () => {
  let component: FashionBundleComponent;
  let fixture: ComponentFixture<FashionBundleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FashionBundleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FashionBundleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
