import {
    AfterViewInit,
    Component,
    ComponentFactory,
    ComponentFactoryResolver,
    ComponentRef,
    OnInit,
    ViewChild,
    ViewContainerRef
} from '@angular/core';
import {FashionBundleComponent} from '../fashion-bundle/fashion-bundle.component';
import {FashionBoardPurchaseComponent} from '../fashion-board-purchase/fashion-board-purchase.component';
import {AuthService} from '../../services/auth.service';
import {TokenStorageService} from '../../services/token-storage.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-fashion-board-clear',
    templateUrl: './fashion-board-clear.component.html',
    styleUrls: ['./fashion-board-clear.component.scss']
})
export class FashionBoardClearComponent implements OnInit, AfterViewInit {
    componentRef: ComponentRef<FashionBundleComponent>;
    componentRefPurchase: ComponentRef<FashionBoardPurchaseComponent>;
    public user: any;

    @ViewChild('fashionContainer', {read: ViewContainerRef}) container;

    constructor(private resolver: ComponentFactoryResolver, public authService: AuthService, public tokenStorage: TokenStorageService, public router: Router) {
    }

    ngOnInit(): void {
        if (this.tokenStorage.getToken()) {
            this.authService.me().subscribe(res => {
                this.user = res;

            });
        }

    }

    ngAfterViewInit(): void {
        const factory: ComponentFactory<FashionBundleComponent> = this.resolver.resolveComponentFactory(FashionBundleComponent);
        this.componentRef = this.container.createComponent(factory);
            this.componentRef.instance.purchaseAction.subscribe(res => {
                if (this.tokenStorage.getUser() && this.tokenStorage.getToken()) {
                    this.container.clear();
                const factoryPurchase: ComponentFactory<FashionBoardPurchaseComponent> = this.resolver.resolveComponentFactory(FashionBoardPurchaseComponent);
                this.componentRefPurchase = this.container.createComponent(factoryPurchase);
                this.componentRefPurchase.instance.user = this.user;
                this.componentRefPurchase.instance.fashionBoard = res;
                }
                else{
                    this.router.navigate(['/pages/login']);
                }
            });
    }

}
