import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FashionBoardClearComponent } from './fashion-board-clear.component';

describe('FashionBoardClearComponent', () => {
  let component: FashionBoardClearComponent;
  let fixture: ComponentFixture<FashionBoardClearComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FashionBoardClearComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FashionBoardClearComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
