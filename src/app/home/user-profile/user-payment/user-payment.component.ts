import {Component, OnInit} from '@angular/core';
import {OrderService} from '../../../services/order.service';
import {Order} from '../../../models/order.model';

@Component({
    selector: 'app-user-payment',
    templateUrl: './user-payment.component.html',
    styleUrls: ['./user-payment.component.scss']
})
export class UserPaymentComponent implements OnInit {
    public orders: Order[] = [];

    constructor(private orderService: OrderService) {
    }

    ngOnInit(): void {
        this.orderService.getOrderByUser().subscribe(res=>{
            this.orders = res;
        })
    }

}
