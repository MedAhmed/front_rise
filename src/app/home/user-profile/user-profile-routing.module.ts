import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {FashionOneComponent} from '../fashion/fashion-one/fashion-one.component';
import {UserProfileComponent} from './user-profile.component';
import {UserInformationDetailsComponent} from './user-information-details/user-information-details.component';
import {UserFashionboardsComponent} from './user-fashionboards/user-fashionboards.component';
import {UserFashionboardsBlankComponent} from './user-fashionboards-blank/user-fashionboards-blank.component';
import {UserPaymentComponent} from './user-payment/user-payment.component';
import {UserSupportComponent} from './user-support/user-support.component';
import {UserParameterComponent} from './user-parameter/user-parameter.component';

const routes: Routes = [

    {
        path: '',
        component: UserProfileComponent,
        children: [
            {
                path: '',
                component: UserInformationDetailsComponent
            },
            {
                path: 'fashionboards',
                component: UserFashionboardsBlankComponent
            },
            {
                path: 'orders',
                component: UserPaymentComponent
            },
            {
                path: 'support',
                component: UserSupportComponent
            },
            {
                path: 'parameter',
                component: UserParameterComponent
            },
            ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UserProfileRoutingModule {
}
