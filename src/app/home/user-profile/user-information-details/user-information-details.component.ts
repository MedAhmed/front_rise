import {Component, ElementRef, NgZone, OnInit, ViewChild} from '@angular/core';
import {CountryISO, PhoneNumberFormat, SearchCountryField} from 'ngx-intl-tel-input';
import {TokenStorageService} from '../../../services/token-storage.service';
import {AuthService} from '../../../services/auth.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ApiService, Maps} from '../../../services/api.service';
import {ToastrService} from 'ngx-toastr';
import {formatDate} from '@angular/common';

@Component({
    selector: 'app-user-information-details',
    templateUrl: './user-information-details.component.html',
    styleUrls: ['./user-information-details.component.scss']
})
export class UserInformationDetailsComponent implements OnInit {

    separateDialCode = false;
    SearchCountryField = SearchCountryField;
    CountryISO = CountryISO;
    PhoneNumberFormat = PhoneNumberFormat;
    preferredCountries: CountryISO[] = [CountryISO.UnitedStates, CountryISO.UnitedKingdom];
    public user: any;
    firstFormGroup: FormGroup;
    submitted: boolean;
    @ViewChild('search')
    public searchElementRef: ElementRef;
    thirdFormGroup: any;
    secondFormGroup: any;
    today: Date = new Date();
    birthDate: Date = new Date();

    constructor(private toastrService: ToastrService, private tokenStorage: TokenStorageService, private authService: AuthService, private formBuilder: FormBuilder, private ngZone: NgZone, apiService: ApiService) {
        apiService.api.then(maps => {
            this.initAutocomplete(maps);
        });

    }

    ngOnInit(): void {
        this.firstFormGroup = this.formBuilder.group({
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            place: ['', Validators.required],
            birthDate: ['', Validators.required],
            email: ['', [Validators.required, Validators.email]],
            phone: ['', Validators.required]
        });
        if (this.tokenStorage.getToken()) {
            this.authService.me().subscribe(res => {
                this.user = res;
                this.firstFormGroup.get('firstName').patchValue(this.user.firstName);
                this.firstFormGroup.get('lastName').patchValue(this.user.lastName);
                this.firstFormGroup.get('phone').patchValue(this.user.phoneNumber);
                this.firstFormGroup.get('place').patchValue(this.user.place);
                this.firstFormGroup.get('birthDate').patchValue(formatDate(this.user.birthDate, 'yyyy-MM-dd', 'en'));
                this.firstFormGroup.get('email').patchValue(this.user.email);
            });
        }
    }

    get f() {
        return this.firstFormGroup.controls;

    }

    initAutocomplete(maps: Maps) {
        let autocomplete = new maps.places.Autocomplete(this.searchElementRef.nativeElement);
        autocomplete.addListener('place_changed', () => {
            this.ngZone.run(() => {
            });
        });
    }
    onSubmit() {
        this.submitted = true;
        const birthDate = this.firstFormGroup.value.birthDate;
        this.birthDate = new Date(birthDate);
        const v = {
            ...this.firstFormGroup.getRawValue(),
            phone: this.firstFormGroup.get('phone').value.internationalNumber
        };
        if (this.firstFormGroup.valid && (this.birthDate < this.today)) {
                this.authService.changeMe(v).subscribe(r => {
                    this.toastrService.success('user updated');
            });

        } else {
            return;
        }
    }
}
