import {Component, OnInit} from '@angular/core';
import {TokenStorageService} from '../../services/token-storage.service';
import {Router} from '@angular/router';
import {environment} from '../../../environments/environment';

@Component({
    selector: 'app-user-profile',
    templateUrl: './user-profile.component.html',
    styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {
    public imageUrl: string;
    public user: any;
    public menu: string = 'profile';
    public api: String = environment.api;

    constructor(public tokenStorage: TokenStorageService, public router: Router) {
        if (router.url.indexOf('fashionboards') > 0) {
            this.menu = 'styleboards';
        } else if (router.url.indexOf('orders') > 0) {
            this.menu = 'payments';
        }else if (router.url.indexOf('support') > 0){
            this.menu = 'support';
        }else if (router.url.indexOf('parameter') > 0){
            this.menu = 'parameter';
        }
        else{
            this.menu = 'profile';

        }
    }

    ngOnInit(): void {
        if (this.tokenStorage.getUser() && this.tokenStorage.getToken()) {
            this.user = this.tokenStorage.getUser();
            this.imageUrl = this.api+'getimage/' + this.tokenStorage.getUser().ImageUrl;
            console.log(this.imageUrl);
        } else {
            this.router.navigate(['/pages/login']);
        }
    }

    changeMenu(s) {
        this.menu = s;
    }

    logout(logout: string) {
        this.tokenStorage.signOut();
        this.router.navigate(['/home/fashion']);
    }
}
