import {Component, OnInit} from '@angular/core';
import {QuizService} from '../../../services/quiz.service';
import {ConfirmationModalComponent} from '../../../shared/components/confirmation-modal/confirmation-modal.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ToastrService} from 'ngx-toastr';
import {AuthService} from '../../../services/auth.service';
import {TokenStorageService} from '../../../services/token-storage.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-user-parameter',
    templateUrl: './user-parameter.component.html',
    styleUrls: ['./user-parameter.component.scss']
})
export class UserParameterComponent implements OnInit {

    constructor(private router:Router,private tokenStorageService: TokenStorageService, private userService: AuthService, private quizService: QuizService, private modalService: NgbModal, private toastrService: ToastrService,) {
    }

    ngOnInit(): void {
    }

    deleteQuizHistory() {
        const modalRef = this.modalService.open(ConfirmationModalComponent, {
            ariaLabelledBy: 'Cart-Modal',
            centered: true,
            windowClass: 'theme-modal cart-modal CartModal'
        });
        modalRef.componentInstance.message = 'you wanna delete your quiz response?';
        modalRef.componentInstance.cancelActionOutput.subscribe(res => {
            modalRef.close();
        });
        modalRef.componentInstance.confirmActionOutput.subscribe(res => {
            modalRef.close();
            this.quizService.deleteQuiz().subscribe(res => {
                this.toastrService.success('Quiz was deleted successfully!');

            });
        });

    }

    deleteAccount() {
        const modalRef = this.modalService.open(ConfirmationModalComponent, {
            ariaLabelledBy: 'Cart-Modal',
            centered: true,
            windowClass: 'theme-modal cart-modal CartModal'
        });
        modalRef.componentInstance.message = 'are you sure??';
        modalRef.componentInstance.cancelActionOutput.subscribe(res => {
            modalRef.close();
        });
        modalRef.componentInstance.confirmActionOutput.subscribe(res => {
            modalRef.close();
            this.userService.deleteUser().subscribe(res => {
                this.toastrService.success(res.message);
                this.router.navigate(['/home/fashion']);

            });
        });
    }


}
