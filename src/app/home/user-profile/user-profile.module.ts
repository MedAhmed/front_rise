import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserProfileRoutingModule } from './user-profile-routing.module';
import {RouterModule} from '@angular/router';
import {UserInformationDetailsComponent} from './user-information-details/user-information-details.component';
import {NgxIntlTelInputModule} from 'ngx-intl-tel-input';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { UserFashionboardsComponent } from './user-fashionboards/user-fashionboards.component';
import {BarRatingModule} from 'ngx-bar-rating';
import { UserFashionboardsBlankComponent } from './user-fashionboards-blank/user-fashionboards-blank.component';
import { FashionboardQuizComponent } from './fashionboard-quiz/fashionboard-quiz.component';
import {MatStepperModule} from '@angular/material/stepper';
import { FashionboardProductsComponent } from './fashionboard-products/fashionboard-products.component';
import {CarouselModule} from 'ngx-owl-carousel-o';
import { UserPaymentComponent } from './user-payment/user-payment.component';
import { UserSupportComponent } from './user-support/user-support.component';
import { UserParameterComponent } from './user-parameter/user-parameter.component';
import {NgbAccordionModule} from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [
    UserInformationDetailsComponent,
    UserFashionboardsComponent,
    UserFashionboardsBlankComponent,
    FashionboardQuizComponent,
    FashionboardProductsComponent,
    UserPaymentComponent,
    UserSupportComponent,
    UserParameterComponent
  ],
  exports: [
  ],
    imports: [
        CommonModule,
        RouterModule,
        UserProfileRoutingModule,
        ReactiveFormsModule,
        NgxIntlTelInputModule,
        BarRatingModule,
        MatStepperModule,
        CarouselModule,
        NgbAccordionModule,
    ]
})
export class UserProfileModule { }
