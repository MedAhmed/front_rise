import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserFashionboardsBlankComponent } from './user-fashionboards-blank.component';

describe('UserFashionboardsBlankComponent', () => {
  let component: UserFashionboardsBlankComponent;
  let fixture: ComponentFixture<UserFashionboardsBlankComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserFashionboardsBlankComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserFashionboardsBlankComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
