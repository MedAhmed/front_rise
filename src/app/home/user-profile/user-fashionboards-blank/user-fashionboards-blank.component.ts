import {
    AfterViewInit,
    Component,
    ComponentFactory,
    ComponentFactoryResolver,
    ComponentRef,
    OnInit,
    ViewChild,
    ViewContainerRef
} from '@angular/core';
import {UserFashionboardsComponent} from '../user-fashionboards/user-fashionboards.component';
import {FashionBundleComponent} from '../../fashion-bundle/fashion-bundle.component';
import {FashionboardQuizComponent} from '../fashionboard-quiz/fashionboard-quiz.component';
import {FashionboardProductsComponent} from '../fashionboard-products/fashionboard-products.component';

@Component({
    selector: 'app-user-fashionboards-blank',
    templateUrl: './user-fashionboards-blank.component.html',
    styleUrls: ['./user-fashionboards-blank.component.scss']
})
export class UserFashionboardsBlankComponent implements AfterViewInit {
    @ViewChild('fashionContainer', {read: ViewContainerRef}) container;
    componentRef: ComponentRef<UserFashionboardsComponent>;
    componentRef1: ComponentRef<FashionboardQuizComponent>;
    componentRef2: ComponentRef<FashionboardProductsComponent>;

    constructor(private resolver: ComponentFactoryResolver) {
    }

    AfterViewInit(): void {
        const factory: ComponentFactory<UserFashionboardsComponent> = this.resolver.resolveComponentFactory(UserFashionboardsComponent);
        this.componentRef = this.container.createComponent(factory);
    }

    ngAfterViewInit(): void {
        const factory: ComponentFactory<UserFashionboardsComponent> = this.resolver.resolveComponentFactory(UserFashionboardsComponent);
        this.componentRef = this.container.createComponent(factory);
        this.componentRef.instance.quizActionoutput.subscribe(res => {
            this.container.clear();
            const factoryQuiz: ComponentFactory<FashionboardQuizComponent> = this.resolver.resolveComponentFactory(FashionboardQuizComponent);
            this.componentRef1 = this.container.createComponent(factoryQuiz);
        });
        this.componentRef.instance.productActionOutput.subscribe(res => {
            this.container.clear();
            const factoryProducts: ComponentFactory<FashionboardProductsComponent> = this.resolver.resolveComponentFactory(FashionboardProductsComponent);

            this.componentRef2 = this.container.createComponent(factoryProducts);
            this.componentRef2.instance.styleBooard = res;

        });
    }

}
