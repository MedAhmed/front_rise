import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FashionboardProductsComponent } from './fashionboard-products.component';

describe('FashionboardProductsComponent', () => {
  let component: FashionboardProductsComponent;
  let fixture: ComponentFixture<FashionboardProductsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FashionboardProductsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FashionboardProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
