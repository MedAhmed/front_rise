import {Component, Input, OnInit} from '@angular/core';
import {StyleboardService} from '../../../services/styleboard.service';
import {Product} from '../../../models/product.model';
import {NewProductSlider} from '../../../shared/data/slider';
import {environment} from '../../../../environments/environment';

@Component({
    selector: 'app-fashionboard-products',
    templateUrl: './fashionboard-products.component.html',
    styleUrls: ['./fashionboard-products.component.scss']
})
export class FashionboardProductsComponent implements OnInit {
    @Input() styleBooard;
    public products:Product[]=[]
    public NewProductSliderConfig: any = NewProductSlider;
    public api: String = environment.api;

    constructor(private styleBoardService: StyleboardService) {
    }

    ngOnInit(): void {
        this.styleBoardService.getProducts({fashionBoardId: this.styleBooard.id}).subscribe(res=>{
            this.products=res;
        });
    }

}
