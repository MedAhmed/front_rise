import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FashionboardQuizComponent } from './fashionboard-quiz.component';

describe('FashionboardQuizComponent', () => {
  let component: FashionboardQuizComponent;
  let fixture: ComponentFixture<FashionboardQuizComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FashionboardQuizComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FashionboardQuizComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
