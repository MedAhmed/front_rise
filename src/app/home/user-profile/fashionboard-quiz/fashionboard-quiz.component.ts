import {Component, OnInit, ViewChild} from '@angular/core';
import {MatStepper} from '@angular/material/stepper';
import {STEPPER_GLOBAL_OPTIONS} from '@angular/cdk/stepper';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {QuizService} from '../../../services/quiz.service';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';

@Component({
    selector: 'app-fashionboard-quiz',
    templateUrl: './fashionboard-quiz.component.html',
    styleUrls: ['./fashionboard-quiz.component.scss'],
    providers: [
        {
            provide: STEPPER_GLOBAL_OPTIONS,
            useValue: {displayDefaultIndicatorType: false},
        },
    ],
})
export class FashionboardQuizComponent implements OnInit {
    firstFormGroup: FormGroup;
    secondFormGroup: FormGroup;
    thirdFormGroup: FormGroup;
    fourthFormGroup: FormGroup;
    requestQuiz: any[] = [];
    counter: number = 0;
    sizes: string[] = ['Petite', 'Tall', 'Mince', 'Curvy', 'Fit', 'Normal'];
    lookTypeJourImage: string[] = ['assets/images/quiz/style/chic.png', 'assets/images/quiz/style/casual.png', 'assets/images/quiz/style/glamour.jpg', 'assets/images/quiz/style/sport.png'];
    @ViewChild('stepper', {static: false}) stepper: MatStepper;
    submittedFirstFormGroup: boolean = false;
    submittedSecondFormGroup: boolean = false;
    submittedThirdFormGroup: boolean = false;
    submittedFourthFormGroup: boolean = false;
    marques: string[] = ['adidas.png', 'bershka.png', 'boohoo.png', 'calvinklein.png', 'chloe.png', 'coach.png', 'fendi.png', 'forever21.png', 'gap.png', 'gucci.png', 'hermes.png', 'jennyfer.png', 'mango.png', 'maxmara.png', 'tommy.png', 'zara.png'];

    constructor(private quizService: QuizService,private formBuilder: FormBuilder, public toastr: ToastrService,public router: Router) {
    }

    ngOnInit(): void {
        this.firstFormGroup = this.formBuilder.group({
            gender: ['', Validators.required]
        });
        this.secondFormGroup = this.formBuilder.group({
            topSize: ['', Validators.required],
            bottomSize: ['', Validators.required],
            shoesSize: ['', Validators.required],
            morphology: this.formBuilder.array([])
        });
        this.thirdFormGroup = this.formBuilder.group({
            lookType: ['', Validators.required],
            look: this.formBuilder.array([])
        });
        this.fourthFormGroup = this.formBuilder.group({
            marque: this.formBuilder.array([])

        });
        const add = this.fourthFormGroup.get('marque') as FormArray;
        for (let i in this.marques) {
            add.push(this.formBuilder.group({
                marqueTypeImage: [],
            }));
        }
        const addSize = this.secondFormGroup.get('morphology') as FormArray;
        for (let i in this.sizes) {
            addSize.push(this.formBuilder.group({
                morphologyBox: [],
            }));
        }
    }

    get formData() {
        return <FormArray> this.thirdFormGroup.get('look');
    }

    get formDataMarque() {
        return <FormArray> this.fourthFormGroup.get('marque');
    }

    get formDataSize() {
        return <FormArray> this.secondFormGroup.get('morphology');
    }

    nothing() {

    }


    nextForm() {
        this.stepper.next();
    }

    onSubmit() {
        this.requestQuiz= [];
        this.submittedFirstFormGroup = true;
        if (this.firstFormGroup.valid) {
            this.stepper.next();
            console.log(this.firstFormGroup.getRawValue());
            this.requestQuiz = [this.firstFormGroup.getRawValue()];
        } else {
            return;
        }
    }

    submitFormBody() {
        this.submittedSecondFormGroup = true;
        if (this.secondFormGroup.valid) {
            const tmp = this.secondFormGroup.get('morphology').value.map(e => e['morphologyBox']);
            const indexesMorphology = this.getIndexesTrue(tmp);
            const resultArr = indexesMorphology.map(i => this.sizes[i]);
            const second = {
                bottomSize: this.secondFormGroup.get('bottomSize').value,
                shoesSize: this.secondFormGroup.get('shoesSize').value,
                topSize: this.secondFormGroup.get('topSize').value,
                morphology: resultArr
            };

            this.requestQuiz.push(second);
            this.stepper.next();
            console.log(this.requestQuiz);
        } else {
            return;
        }
    }

    submitFormLook() {
        this.submittedThirdFormGroup = true;
        if (this.thirdFormGroup.valid) {
            const tmp = this.thirdFormGroup.get('look').value.map(e => e['lookTypeImage']);
            const indexesMorphology = this.getIndexesTrue(tmp);
            const resultArr = indexesMorphology.map(i => this.lookTypeJourImage[i].slice(0, -4));
            const third = {
                lookType: this.thirdFormGroup.get('lookType').value,
                look: resultArr
            };
            this.requestQuiz.push(third);
            this.stepper.next();
        } else {
            return;
        }
    }

    submitFormMarque() {
        this.submittedFourthFormGroup = true;
        if (this.fourthFormGroup.valid) {
            const tmp = this.fourthFormGroup.get('marque').value.map(e => e['marqueTypeImage']);
            const indexesMorphology = this.getIndexesTrue(tmp);
            const resultArr = indexesMorphology.map(i => this.marques[i].slice(0, -4));
            const fourth = {
                marque: resultArr
            };
            this.requestQuiz.push(fourth);
            this.quizService.addQuiz(this.requestQuiz).subscribe(res=>{
                let message = res['status'];
                this.toastr.success(message, 'success');
                window.location.reload();

                this.router.navigate(['/home/profile/fashionboards']);
            });
            this.stepper.next();


        } else {
            return;
        }
    }

    getIndexesTrue(d) {
        const indices = d.flatMap((bool, index) => bool ? index : []);
        return indices;
    }

    get f() {
        return this.firstFormGroup.controls;

    }

    get f2() {
        return this.secondFormGroup.controls;

    }

    get f3() {
        return this.thirdFormGroup.controls;

    }

    get f4() {
        return this.fourthFormGroup.controls;

    }

    changeLook(mode) {
        const add = this.thirdFormGroup.get('look') as FormArray;
        add.clear();
        if (mode === 'morning') {
            this.lookTypeJourImage = ['chic.png', 'casual.png', 'trendy.png', 'sport.png'];
            for (let i in this.lookTypeJourImage) {
                add.push(this.formBuilder.group({
                    lookTypeImage: [],
                }));
            }
        } else {
            this.lookTypeJourImage = ['glamour.jpg', 'sexy.png'];
            for (let i in this.lookTypeJourImage) {
                add.push(this.formBuilder.group({
                    lookTypeImage: [],
                }));
            }

        }
    }


}
