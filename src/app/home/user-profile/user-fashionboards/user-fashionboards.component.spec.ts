import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserFashionboardsComponent } from './user-fashionboards.component';

describe('UserFashionboardsComponent', () => {
  let component: UserFashionboardsComponent;
  let fixture: ComponentFixture<UserFashionboardsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserFashionboardsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserFashionboardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
