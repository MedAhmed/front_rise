import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {StyleboardService} from '../../../services/styleboard.service';
import {Styleboard} from '../../../models/styleboard.model';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ConfirmationModalComponent} from '../../../shared/components/confirmation-modal/confirmation-modal.component';

@Component({
    selector: 'app-user-fashionboards',
    templateUrl: './user-fashionboards.component.html',
    styleUrls: ['./user-fashionboards.component.scss']
})
export class UserFashionboardsComponent implements OnInit {
    public styleBoards: Styleboard[] = [];
    public countStyleBoard;
    @Output() quizActionoutput = new EventEmitter<any>();
    @Output() productActionOutput = new EventEmitter<Styleboard>();

    constructor(private styleBoardService: StyleboardService, private modalService: NgbModal
    ) {
    }

    ngOnInit(): void {
        this.styleBoardService.getAllStyleBoard().subscribe(res => {
            this.styleBoards = res;
            this.countStyleBoard = this.styleBoards.filter(styleBoard => styleBoard.clientActivation === false).length;
        });
    }

    activateFashionBoard() {
        const modalRef = this.modalService.open(ConfirmationModalComponent, {
            ariaLabelledBy: 'Cart-Modal',
            centered: true,
            windowClass: 'theme-modal cart-modal CartModal'
        });
        modalRef.componentInstance.message = 'Are you sure gonna activate the fashionbundle';
        modalRef.componentInstance.cancelActionOutput.subscribe(res => {
            modalRef.close();
        });
        modalRef.componentInstance.confirmActionOutput.subscribe(res => {
            modalRef.close();
            this.quizActionoutput.emit();
        });
    }

    viewProducts(styleBoards: Styleboard) {
        this.productActionOutput.emit(styleBoards);
    }
}
