import {Component, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators} from '@angular/forms';
import {AuthService} from '../../../services/auth.service';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {ImageService} from '../../../services/image.service';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
    form: FormGroup;
    submitted: boolean = false;
    private file: any;
    today: Date = new Date();
    birthDate: Date = new Date();

    constructor(private formBuilder: FormBuilder, private authService: AuthService, private toastrService: ToastrService, private router: Router, private imageService: ImageService) {
    }

    checkPasswords: ValidatorFn = (group: AbstractControl): ValidationErrors | null => {
        let pass = group.get('password').value;
        let confirmPass = group.get('confirmPassword').value;
        return pass === confirmPass ? null : {notSame: true};
    };


    ngOnInit(): void {
        this.form = this.formBuilder.group({
            username: new FormControl('', [Validators.required]),
            email: new FormControl('', [Validators.required, Validators.email]),
            lastName: new FormControl('', [Validators.required]),
            firstName: new FormControl('', [Validators.required]),
            password: new FormControl('', [Validators.required, Validators.minLength(8)]),
            confirmPassword: new FormControl('', [Validators.required, Validators.minLength(8)]),
            birthDate: new FormControl('', [Validators.required])
        });
    }

    get f() {
        return this.form.controls;

    }

    onSubmit() {
        this.submitted = true;
        const username = this.form.value.username;
        const email = this.form.value.email;
        const password = this.form.value.password;
        const firstName = this.form.value.firstName;
        const lastName = this.form.value.lastName;
        const birthDate = this.form.value.birthDate;
        this.birthDate = new Date(birthDate);
        if (this.form.valid) {
            this.imageService.uploadFile(this.file).subscribe(res => {
                this.authService.register(username, email, password, lastName, firstName, res.filename, this.birthDate).subscribe(r => {
                    this.toastrService.success(r.message);
                });
            });

        } else {
            return;
        }
        this.router.navigate(['/pages/login']);
    }

    onChange(event) {
        this.file = event.target.files[0];
    }
}
