import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../../services/auth.service';
import {TokenStorageService} from '../../../services/token-storage.service';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {FacebookService} from '../../../services/facebook.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    isLoggedIn: boolean = false;
    isLoginFailed = false;
    form: FormGroup;
    submitted = false;
    roles = [];

    constructor(public tokenStorage: TokenStorageService,
                private formBuilder: FormBuilder, public authService: AuthService,
                public toastr: ToastrService, private router: Router, public facebookService: FacebookService) {
    }

    ngOnInit(): void {
        this.form = this.formBuilder.group({
            email: new FormControl('', [Validators.required]),
            password: new FormControl('', [Validators.required, Validators.minLength(8)])
        });
    }

    get f() {
        return this.form.controls;
    }

    onSubmit() {
        console.log('hello');
        this.submitted = true;

        // stop here if form is invalid
        if (this.form.invalid) {
            return;
        }
        const username = this.form.value.email;
        const password = this.form.value.password;
        this.authService.login(username, password).subscribe(res => {
            this.tokenStorage.saveToken(res.accessToken);
            this.tokenStorage.saveUser(res);
            this.isLoginFailed = false;
            this.isLoggedIn = true;
            this.roles = this.tokenStorage.getUser().roles;
            this.router.navigate(['/home/fashion']);
        }, error => {
            this.toastr.error('mot de passe incorrect', 'Alert');
        });
        // display form values on success
    }

    loginFacebook() {
        this.facebookService.loginFacebook().subscribe(res => {
            console.log(res);
        });
    }
}
