import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-confirmation-modal',
  templateUrl: './confirmation-modal.component.html',
  styleUrls: ['./confirmation-modal.component.scss']
})
export class ConfirmationModalComponent implements OnInit {
  @Output() cancelActionOutput = new EventEmitter<any>();
  @Output() confirmActionOutput = new EventEmitter<any>();
  @Input() message
  constructor() { }

  ngOnInit(): void {
  }

  confirmAction() {
    this.confirmActionOutput.emit();
  }

  closeAction() {
    this.cancelActionOutput.emit();
  }
}
