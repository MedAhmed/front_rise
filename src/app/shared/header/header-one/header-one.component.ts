import {Component, OnInit, Input, HostListener} from '@angular/core';
import {TokenStorageService} from '../../../services/token-storage.service';
import {AuthService} from '../../../services/auth.service';

@Component({
    selector: 'app-header-one',
    templateUrl: './header-one.component.html',
    styleUrls: ['./header-one.component.scss']
})
export class HeaderOneComponent implements OnInit {

    @Input() class: string;
    @Input() themeLogo: string = 'assets/images/icon/logo.png'; // Default Logo
    @Input() topbar: boolean = true; // Default True
    @Input() sticky: boolean = false; // Default false
    @Input() username: string = '';
    public stick: boolean = false;
    public isLoggedIn: boolean;

    constructor(public tokenStorageService: TokenStorageService, public authService: AuthService) {
        this.username = this.tokenStorageService.getUser().username;
        this.isLoggedIn = !this.tokenStorageService.getToken();
    }

    ngOnInit(): void {
    }

    // @HostListener Decorator
    @HostListener('window:scroll', [])
    onWindowScroll() {
        let number = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
        if (number >= 150 && window.innerWidth > 400) {
            this.stick = true;
        } else {
            this.stick = false;
        }
    }

    logout() {
        this.tokenStorageService.signOut();
        this.username = '';
        window.location.reload();

    }
}
