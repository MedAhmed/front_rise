import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class FacebookService {

    public api: string = environment.api;
    public user: any;
    public httpOptions = {
        headers: new HttpHeaders({'Content-Type': 'application/json'})
    };

    constructor(private http: HttpClient) {
    }

    loginFacebook(): Observable<any> {
        return this.http.get(this.api + 'auth/facebook', this.httpOptions);
    }

}
