import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {BehaviorSubject, Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    public api: string = environment.api;
    public user: Observable<any>;
    public httpOptions = {
        headers: new HttpHeaders({'Content-Type': 'application/json'})
    };
    private imageUrl: any;
    private userSubject: BehaviorSubject<any>;

    constructor(private http: HttpClient) {
        this.userSubject = new BehaviorSubject<any>(null);
        this.user = this.userSubject.asObservable();
    }

    public get userValue(): any {
        return this.userSubject.value;
    }

    login(username: string, password: string): Observable<any> {
        return this.http.post(this.api + 'auth/signin', {
            username,
            password
        }, this.httpOptions);
    }

    register(username: string, email: string, password: string, lastName: string, firstName: string, imageUrl: string, birthDate: any): Observable<any> {
        return this.http.post(this.api + 'auth/signup', {
            username,
            email,
            password,
            firstName,
            lastName,
            imageUrl,
            birthDate
        }, this.httpOptions);
    }

    me(): Observable<any> {
        return this.http.post(this.api + 'auth/me', {}, this.httpOptions);
    }

    changeMe(user): Observable<any> {
        return this.http.post(this.api + 'auth/changeme', user, this.httpOptions);
    }
    deleteUser(): Observable<any> {
        return this.http.post(this.api + 'auth/delete', {}, this.httpOptions);
    }
}
