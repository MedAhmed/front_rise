import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Fashionboard} from '../models/fashionboard.model';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class QuizService {
    public api: string = environment.api;
    public user: any;
    public httpOptions = {
        headers: new HttpHeaders({'Content-Type': 'application/json'})
    };

    constructor(private http: HttpClient) {
    }

    addQuiz(quiz): Observable<any> {
        return this.http.post<any>(this.api + 'quiz/add', {'quiz': quiz}, this.httpOptions);

    }
    deleteQuiz(): Observable<any> {
        return this.http.post<any>(this.api + 'quiz/delete', {}, this.httpOptions);

    }
}
