import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  public api: string = environment.api;
  public user: any;
  public httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  constructor(private http: HttpClient) {
  }
  stripePayment(stripeData): Observable<any> {
    return this.http.post(this.api + 'stripe/payment', stripeData   , this.httpOptions);
  }
  paypalPayment(data): Observable<any> {
    return this.http.post(this.api + 'paybal/payment', data   , this.httpOptions);
  }
}
