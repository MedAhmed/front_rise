import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Fashionboard} from '../models/fashionboard.model';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FashionboardService {

  public api: string = environment.api;
  public user: any;
  public httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  constructor(private http: HttpClient) {
  }
  getAllFashionBoard(): Observable<Fashionboard[]> {
    return this.http.get<Fashionboard[]>(this.api + 'fashionbundle/list', this.httpOptions);
  }
}
