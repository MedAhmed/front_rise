import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Order} from '../models/order.model';

@Injectable({
    providedIn: 'root'
})
export class OrderService {

    public api: string = environment.api;
    public user: any;
    public httpOptions = {
        headers: new HttpHeaders({'Content-Type': 'application/json'})
    };

    constructor(private http: HttpClient) {
    }

    getOrderByUser(): Observable<Order[]> {
        return this.http.post<Order[]>(this.api + 'order/list', {}, this.httpOptions);
    }
}
