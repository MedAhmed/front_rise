import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Image} from '../models/image.model';

@Injectable({
  providedIn: 'root'
})
export class ImageService {
  public api: string = environment.api;
  public user: any;
  public httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded'})
  };

  constructor(private http: HttpClient) {
  }

  public uploadFile(file): Observable<Image> {
    const formData = new FormData();
    formData.append('image', file);
    return this.http.post<Image>(this.api + 'uploading', formData);
  }
}
