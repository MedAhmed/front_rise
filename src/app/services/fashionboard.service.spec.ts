import { TestBed } from '@angular/core/testing';

import { FashionboardService } from './fashionboard.service';

describe('FashionboardService', () => {
  let service: FashionboardService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FashionboardService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
