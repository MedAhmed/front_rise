import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Styleboard} from '../models/styleboard.model';
import {Product} from '../models/product.model';

@Injectable({
    providedIn: 'root'
})
export class StyleboardService {
    public api: string = environment.api;
    public user: any;
    public httpOptions = {
        headers: new HttpHeaders({'Content-Type': 'application/json'})
    };

    constructor(private http: HttpClient) {
    }

    getAllStyleBoard(): Observable<Styleboard[]> {
        return this.http.get<Styleboard[]>(this.api + 'fashionboard/list', this.httpOptions);
    }

    getProducts(styleBoard): Observable<Product[]> {
        return this.http.post<Product[]>(this.api + 'fashionboard/products-parsed', styleBoard, this.httpOptions);
    }
}
