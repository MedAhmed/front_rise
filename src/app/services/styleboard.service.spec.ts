import { TestBed } from '@angular/core/testing';

import { StyleboardService } from './styleboard.service';

describe('StyleboardService', () => {
  let service: StyleboardService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StyleboardService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
