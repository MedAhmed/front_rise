export class Order {
    id: string;
    transactionid: string;
    transactiontype: string;
    fullName: string;
    amount: number;
    cardType: string;
    createdAt: Date;

}
