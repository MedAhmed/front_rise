export class Image {
  public fieldname: string;
  public originalname: string;
  public encoding: string;
  public mimetype: string;
  public destination: string;
  public filename: string;
  public path: string;
  public size: number;
}
