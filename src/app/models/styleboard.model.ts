export class Styleboard {
    clientActivation: boolean;
    adminValidation: boolean;
    createdAt: Date;
    userId: string;
}
