export class Product {
    id: string;
    name: string;
    brand: string;
    price: number;
    url: string;
    gender: string;
    size: string;
    color: string;
    imageUrl: string;
}
