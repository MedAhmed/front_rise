export class Fashionboard {
    id: string;
    fashionboardnumber: number;
    name: string;
    description: string;
    promotionstatus: boolean;
    promotion: number;
    price: number;
}
